//package com.example.pdfgeneratorclient.redis.models;
//
//
//import lombok.Builder;
//import lombok.Data;
//import org.springframework.data.redis.core.RedisHash;
//import org.springframework.data.redis.core.index.Indexed;
//
//import javax.persistence.Id;
//
//
//@Data
//@Builder
//@RedisHash("user")
//public class RedisUser {
//
//    @Id
//    private Long id;
//
//    @Indexed
//    private String userId;
//
//
//}
//