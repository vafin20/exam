package com.example.pdfgeneratorserver.generators;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.UUID;

public class EnrolledGenerator {

    public static void generate(String content) throws DocumentException, FileNotFoundException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(new File(content + " 2.pdf")));

        document.open();

        Font font = FontFactory.getFont(FontFactory.HELVETICA, 50, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph(content + " enrolled the university.", font);

        document.add(paragraph);
        document.close();
    }
}
