package com.example.pdfgeneratorserver.generators;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.UUID;

public class KickOutGenerator {

    public static void generate(String content) throws DocumentException, FileNotFoundException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(new File(content + " 1.pdf")));

        document.open();

        Font font = FontFactory.getFont(FontFactory.HELVETICA, 30, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph(content + " kicked out of university.", font);

        document.add(paragraph);
        document.close();
    }
}
